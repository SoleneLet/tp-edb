#!/bin/bash
#Script du réseau: informations + test.
#Etablissement des variables

addressnetwork=$(cat /etc/network/interfaces | grep address | cut -d " " -f2 )
gatewaynetwork=$(cat /etc/network/interfaces | grep gateway | cut -d " " -f2 )
dnsnetwork=$(cat /etc/network/interfaces | grep dns-nameservers | cut -d " " -f2 )
addressip=$(cat /etc/network/interfaces | grep address | cut -d " " -f2 | cut -d "/" -f1)

while (true)
do
echo " "

read -p "Que vous voulez faire ?
1) pour consulter les informations réseaux
2) pour tester le réseau
3) pour retourner au menu général

" -n 1  choixreseau
echo ""
#commande case pour choisir parmis des optiosn pré-établies
case $choixreseau in 
	#pour option 1 = info réseaux
	1)
	

	#affichage des variables réseaux
	echo "L'adresse ip de votre serveur est : "$addressnetwork""
	echo "L'adresse de votre passerelle est : "$gatewaynetwork""
	echo "L'adresse de votre DNS est : "$dnsnetwork""
	;; #fin de l'option 1
2)
#pour option 2 = test réseau
#Test localhost
ping -c 1 127.0.0.1 1> /dev/null 2>&1

testlocalhost=$?
if [ $testlocalhost = 0 ]
	then
		echo "L'adresse locale 127.0.0.1 est fonctionnelle"
	else
		echo "L'adresse locale 127.0.0.1 ne répond pas"
	fi
#Test adress ip
ping -c 1 "$addressip" 1> /dev/null 2>&1

testaddress=$?
if [ $testaddress = 0 ]
	then
		echo "L'adresse ip "$addressip" est fonctionnelle"
	else
		echo "L'adresse ip "$addressip" ne répond pas"
	fi
#Test installation outils dig
dig -v 1> /dev/null 2>&1
statusdig=$?
case $statusdig in
	1) 
		apt-get install dnsutils > /dev/null
		;;
esac

#Test passerelle
ping -c 1 "$gatewaynetwork" 1> /dev/null 2>&1
testgateway=$?
if [ $testgateway = 0 ]
	then
		echo "La passerelle "$gatewaynetwork" est fonctionnelle"
	else
		echo "La passerelle "$gatewaynetwork" ne répond pas"
	fi


	
#Test résolution DNS

TestDNS=$(dig +short www.google.com | wc -l)
if [ $TestDNS -gt '0' ]
then
	echo "La résolution DNS fonctionne"
else
	echo "Problème de résolution DNS"
fi
;;

3) 
#pour option retourner menu général
break
;;


esac
done
