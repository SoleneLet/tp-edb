#!/bin/bash
#Nom auteur : S.L
#Date création : 28/04/2021
#Date dernière modif :
#Description du script : Crée un uilisateur Supervision, s'il existe pas déjà
#Invocation du script : ./AddSupervision.sh

#Test est-ce que le dossier /opt/supervision/scripts existe
test -e /opt/supervision/scripts
TestDossier=$?
        if [ $TestDossier -eq '0' ]
        then
                echo "Le dossier /opt/supervision/scripts existe."
        else
                echo "Le dossier /opt/supervision/scripts n'existe pas"
                sudo -u Supervision mkdir -p /opt/supervision/scripts
                if [ $? -eq 0 ]
                then
                        echo "Le dossier /opt/supervision/scripts a été créé."
                fi

        fi

cd /opt/supervision/scripts

##Test installation outils git
git pull > /dev/null
statusgit=$?
if [ $statusgit -ne 0 ]
	then
                apt-get install git -y  > /dev/null
                
fi

#Clone du Git pour récupérer les scripts

git clone  https://gitlab.com/SoleneLet/tp-edb.git

#Test est-ce que l'utilisateur Supervision existe
grep "Supervision" /etc/passwd
TestUser=$?
	if [ $TestUser -eq '0' ]
	then
		echo "Lutilisateur Supervision existe"
	else
		echo "L'utilisateur Supervision n'existe pas"
		useradd -s /opt/supervision/scripts/tp-edb/supervision.sh Supervision -g users
		if [ $? -eq 0 ]
		then
                        echo "L'utilisateur Supervision a été créé"
			grep "Supervision" /etc/passwd
               fi

	fi

#Accorde droits d'exécution sur tous les fichiers du dossier
chown Supervision /opt/supervision/scripts/tp-edb/*
chmod u+x /opt/supervision/scripts/tp-edb/*

#Création du fichier  /etc/sudoers.d/supervision pour autoriser les commandes root
echo " # Liste les commandes autorisees aux superviseurs
Cmnd_Alias SUPERVISION = /sbin/reboot, /sbin/ip, /usr/bin/chage, /bin/chmod, /bin/echo, /bin/systemctl, /usr/sbin/groupadd, /usr/sbin/useradd, /usr/bin/passwd, /etc/init.d/apache2 *, /usr/bin/apt-get *
# Autorise le superviseur a lancer les commandes precedentes sans saisir de mot de passe
Supervision ALL=NOPASSWD:SUPERVISION " > /etc/sudoers.d/Supervision

# Déplacement dans le dossier de scripts
cd /opt/supervision/scripts/tp-edb
git pull
