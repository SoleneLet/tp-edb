#!/bin/bash
#Nom auteur : S.L
#Date création : 27/04/2021
#Date dernière modif :
#Description du script : Affiche la liste des utilisateurs/groupes en 2 listes (standard et système)
#Invocation du script : ./AffichUser.sh

read -p "souhaitez-vous afficher les groupes ou les utilisateurs ? (g ou u) " rep

if [ $rep = "g" -o $rep =  "G" ]
then
	groupsys="Voici la liste des groupes systemes :\n"
        groupstd="Voici la liste des groupes standard :\n"
        # On boucle sur chaque ligne du fichier /etc/group
        while read -r ligne
        do
                # Isoler le GID
                gid=$(echo $ligne | cut -d: -f3)
                # Isoler le Nom
                nom=$(echo $ligne | cut -d: -f1)
                # Si uid < 500 => Utilisateur systeme
                if test "$gid" -lt "500"
                then
			 # Ajouter le nom a la liste
                        groupsys="${groupsys}${nom}, "
                else
                        # Ajouter le nom a la liste
                        groupstd="${groupstd}${nom}, "
                fi
        done < /etc/group
	# Affichage de la liste
        echo -e "$groupsys"
        echo ""
        echo -e "$groupstd"


elif [ $rep = "u" -o  $rep = "U" ]
then
	usersys="Voici la liste des utilisateurs systemes :\n"
	userstd="Voici la liste des utilisateurs standard :\n"
	# On boucle sur chaque ligne du fichier /etc/passwd
	while read -r ligne
	do
		# Isoler l'UID
		uid=$(echo $ligne | cut -d: -f3)
		# Isoler le Nom
		nom=$(echo $ligne | cut -d: -f1)
		# Si uid < 500 => Utilisateur systeme
		if test "$uid" -lt "500"
		then
			# Ajouter le nom a la liste
			usersys="${usersys}${nom}, "
		else
			# Ajouter le nom a la liste
			userstd="${userstd}${nom}, "
		fi
	done < /etc/passwd
	# Affichage de la liste
	echo -e "$usersys"
	echo ""
	echo -e "$userstd"
else echo "Réponse incorrecte"
fi


