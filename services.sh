#!/bin/bash

#Choix service
read -p "Choississez le service que vous souhaitez démarrer: 1 - apache2 ou 2 - postfix: 
" -n 1  choix

echo "Redémarrage du serveur $choix en cours, veuillez patienter..."

case $choix in
	1)

	#Script de redémarrage du serveur apache2
	systemctl status apache2 1> /dev/null 2>&1
	statusapache=$?
	echo $statusapache

	if test "$statusapache" != "0"
	then
	#Installation du service apache2 si celui ci n'est pas encore existant + redémarrage
		sudo apt-get install apache2
		sudo chmod 777 /etc/apache2/apache2.conf
		sudo echo "ServerName localhost" >> /etc/apache2/apache2.conf 
		sudo chmod 644 /etc/apache2/apache2.conf
		sudo systemctl start apache2
		sudo systemctl reload apache2
		echo "installation"
	

	else

		#Redémarrage de apache2 si celui ci est déjà installé
		sudo systemctl start apache2
		sudo systemctl reload apache2
		echo "redemarrage"

	fi

		sudo systemctl status apache2 1> /dev/null 2>&1
		statusapache2=$?


		#Vérification que le redémarrage a bien fonctionné
		if test "$statusapache2" = "0"
		then
			echo "Le serveur apache a été redémarré et est fonctionnel"
	
		else 
	 
			echo "Le serveur apache n'est pas fonctionnel, veuillez contactez un administrateur réseau"
	
		fi

	
	;;

	2) 


#Script de redémarrage du serveur postfix
postfix -v
statuspostfix=$?

if test "$statuspostfix" != "0"
then
#Installation du service apache2 si celui ci n'est pas encore existant
	sudo apt-get install postfix
	sudo systemctl reload postfix
	service postfix status 1> /dev/null 2>&1
	statuspostfix2=$?
else

	#Redémarrage de postfix si celui ci est déjà installé
	sudo systemctl start postfix
	sudo systemctl reload postfix
	service postfix status 1> /dev/null 2>&1
	statuspostfix2=$?
fi

	#Vérification que le redémarrage a bien fonctionné
	if test "$statuspostfix" = "0"
	then
		echo "Le serveur postfix a été redémarré et est fonctionnel"
	
	else 
	 
		echo "Le serveur postfix n'est pas fonctionnel, veuillez contactez un administrateur réseau"
	
	fi
;;

esac
