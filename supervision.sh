#!/bin/bash

#author :Quentin, Solène & Alexandra

cd /opt/supervision/scripts/tp-edb/

show_menu (){
	while (true)
	do
		echo "What do you want to do?"
		echo " "
		echo "1. Users info"
		echo "2. Services info"
		echo "3. Networks info"
		echo "4. Reboot server"
		echo "5. Quit"
		echo " "
		read -p "you choose: " choise
		echo " "

		case $choise in
			"1")
				echo "a. Creation users (or groups)"
				echo "b. Print users (or groups)"
				echo "c. Password change"
				read -p "you choose: " secondchoise
				
				case $secondchoise in
					"a")
						 bash AddUser.sh
					;;
					"b")
						 bash AffichUser.sh
					;;
					"c")
						 bash ChangePass.sh
					;;
				esac
			;;
			"2")
				bash services.sh
			;;
			"3")
				bash network.sh
			;;
			"4")
				echo "Your server will reboot now"
				sudo reboot
			;;
			"5")
				echo "Goodbye!"
				break 
			;;
		esac
		sleep 3
	done
	}

echo "Welcome!"


show_menu


