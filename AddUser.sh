#!/bin/bash
#Nom auteur : S.L
#Date création : 27/04/2021
#Date dernière modif : 28/04/2021
#Description du script : Ajout un utilisateur ou un groupe selon réponse de l'utilisateur.
#			Le GID et/ou UID est demandé.
#Invocation du script : ./AddUser.sh

read -p "Souhaitez-vous créer un groupe ou un utilisateur ? (g ou u) : " rep

if [ $rep = "g" -o $rep = "G" ]
then
	echo "Vous souhaitez créer un groupe."
	echo "Entrez le GID. (Entre 0 et 499 si groupe système.
	Supérieur à 500 si groupe standard.)"
	read gid
	read -p "Entrez le nom du groupe à créer : " gname
	sudo groupadd -g $gid $gname
		if [ $? -eq 0 ]
		then
			echo "Groupe créé"
		fi
elif [ $rep = "u" -o $rep = "U" ]

then
	echo "Vous souhaitez créer un utilisateur."
	echo "Entrez le GID. (Entre 0 et 499 si groupe système.
	Supérieur à 500 si groupe standard.)"
        read gid
        read -p "Entrez le nom de l'utilisateur à créer : " username
        echo "Entre l'UID. (Entre 0 et 999 si utilisateur système.
	Supérieur à 1000 si utilisateur standard.)"
	read uid
	sudo useradd -g $gid -u $uid $username
	       if [ $? -eq 0 ]
               then
                        echo "Utilisateur créé."
               fi

else echo "Réponse incorrecte"
fi
